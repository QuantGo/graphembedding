
import numpy as np

from ge.classify import read_node_label, Classifier
from ge import Node2Vec
from sklearn.linear_model import LogisticRegression

import matplotlib.pyplot as plt
import networkx as nx
from sklearn.manifold import TSNE

import matplotlib.pyplot as plt
plt.rcParams["font.sans-serif"]=["SimHei"]
plt.rcParams["axes.unicode_minus"]=False


def evaluate_embeddings(embeddings):
    X, Y = read_node_label('../data/wiki/wiki_labels.txt')
    tr_frac = 0.8
    print("Training classifier using {:.2f}% nodes...".format(
        tr_frac * 100))
    clf = Classifier(embeddings=embeddings, clf=LogisticRegression())
    clf.split_train_evaluate(X, Y, tr_frac)


def plot_embeddings(embeddings,):
    X, Y = read_node_label('../data/wiki/wiki_labels.txt')

    emb_list = []
    for k in X:
        emb_list.append(embeddings[k])
    emb_list = np.array(emb_list)

    model = TSNE(n_components=2)
    node_pos = model.fit_transform(emb_list)

    color_idx = {}
    for i in range(len(X)):
        color_idx.setdefault(Y[i][0], [])
        color_idx[Y[i][0]].append(i)

    for c, idx in color_idx.items():
        plt.scatter(node_pos[idx, 0], node_pos[idx, 1], label=c)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    # G=nx.read_edgelist('../data/wiki/Wiki_edgelist.txt',
    #                      create_using = nx.DiGraph(), nodetype = None, data = [('weight', int)])

    G = nx.read_edgelist('../data/sector_corr_duel.txt',
                         create_using=nx.DiGraph(), nodetype=None, data=[('weight', float)])

    model = Node2Vec(G, walk_length=10, num_walks=1000000,
                     p=0.25, q=4, workers=1, use_rejection_sampling=0)
    model.train(window_size = 5, iter = 10, embed_size=30)
    embeddings=model.get_embeddings()

    id_name = ["石油石化指数",
               "煤炭指数  ",
               "有色金属指数",
               "电力及公用事业指数",
               "钢铁指数  ",
               "基础化工指数",
               "建筑指数  ",
               "建材指数  ",
               "轻工制造指数",
               "机械指数  ",
               "电力设备及新能源指数",
               "国防军工指数",
               "汽车指数  ",
               "商贸零售指数",
               "消费者服务指数",
               "家电指数  ",
               "纺织服装指数",
               "医药指数  ",
               "食品饮料指数",
               "农林牧渔指数",
               "银行指数  ",
               "非银行金融指数",
               "房地产指数 ",
               "交通运输指数",
               "电子指数  ",
               "通信指数  ",
               "计算机指数 ",
               "传媒指数  ",
               "综合指数  ",
               "综合金融指数"]
    id_name = [i.split('指数')[0] for i in id_name]

    emb_list = []
    for k in embeddings:
        emb_list.append(embeddings[k])
    emb_list = np.array(emb_list)

    # np.save('emb_list_embs50_1e6.npy', emb_list)
    # emb_list = np.load('result/emb_list_embs30_1e6.npy')

    tsne = TSNE(n_components=2, perplexity=25)
    node_pos = tsne.fit_transform(emb_list)
    plt.scatter(node_pos[:, 0], node_pos[:, 1])  # 将对降维的特征进行可视化
    for i in range(30):
        plt.text(node_pos[i, 0], node_pos[i, 1], str(id_name[i]))
    plt.show()


